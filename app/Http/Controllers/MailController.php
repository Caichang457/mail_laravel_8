<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\TestMail;

class MailController extends Controller
{
    public function sendEmail(){
        $details=[
            'title'=>'Mail from Candra',
            'body'=>'This is for testing mail from gmail.'
        ];

        Mail::to("liaocandra456@yahoo.com")->send(new TestMail($details));
        return "Email Sent";
    }
}
